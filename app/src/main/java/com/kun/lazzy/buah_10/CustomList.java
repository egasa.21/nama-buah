package com.kun.lazzy.buah_10;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomList extends ArrayAdapter {

    private Context context;
    private String[] namabuah;
    private  int[] gambarbuah;

    public CustomList( Context context1, String[] namabuah, int[] gambarbuah) {
        super(context1, R.layout.list_item, namabuah);
        this.context = context1;
        this.namabuah = namabuah;
        this.gambarbuah = gambarbuah;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater;
        View view = LayoutInflater.from(context).inflate(R.layout.list_item,parent, false);

        TextView tvnamabuah = view.findViewById(R.id.tvnama);
        ImageView imageView = view.findViewById(R.id.listbuah);

        tvnamabuah.setText(namabuah[position]);
        imageView.setImageResource(gambarbuah[position]);
        return view;
    }
}
