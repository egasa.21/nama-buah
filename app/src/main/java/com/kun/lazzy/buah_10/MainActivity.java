package com.kun.lazzy.buah_10;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView list;
    String[]  namabuah = {
            "Alpukat", "Apel","Ceri","Durian", "Jambu Air", "Manggis", "Strawberry"

    };
    int[] gambarbuah = {
      R.drawable.alpukat, R.drawable.apel, R.drawable.ceri, R.drawable.durian, R.drawable.jambuair, R.drawable.manggis, R.drawable.strawberry
    };

    int[] suarabuah = {
            R.raw.alpukat, R.raw.apel, R.raw.ceri, R.raw.durian, R.raw.jambu_air, R.raw.manggis, R.raw.strawberry
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = findViewById(R.id.listview);
        CustomList adapter = new CustomList(MainActivity.this, namabuah, gambarbuah);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MediaPlayer.create(MainActivity.this, suarabuah[i]).start();
            }
        });
    }
}
